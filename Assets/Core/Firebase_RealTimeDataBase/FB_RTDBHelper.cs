using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using System.Threading.Tasks;

public class FB_RTDBHelper
{
    public static FB_RTDBHelper Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new FB_RTDBHelper();
                // Get the root reference location of the database.
                _instance._database = FirebaseDatabase.DefaultInstance;
            }
            return _instance;
        }
    }
    static FB_RTDBHelper _instance;


    public FirebaseDatabase _database { private set; get; }
    Dictionary<string, DatabaseReference> cachedReference = new Dictionary<string, DatabaseReference>();

    public void SaveData(string reference, string itemKey, object dataStore)
    {
        _database.GetReference(reference).Child(itemKey).SetRawJsonValueAsync(JsonUtility.ToJson(dataStore));
    }
    public void UpdateSpecifiedData(string reference, string itemKey, object newData, bool newDataIfNotExist = false)
    {
        var currentReff = _database.GetReference(reference);
        currentReff.GetValueAsync().ContinueWith(task =>
        {
            if (task.Exception == null || newDataIfNotExist)
            {
                if (newData.GetType().IsPrimitive || newData is string)
                {
                    currentReff.Child(itemKey).SetValueAsync(newData);
                }
                else
                {
                    SaveData(reference, itemKey, newData);
                }
                Debug.Log("Update Data");
            }
            else
            {
                Debug.Log("Pass it-Update");
            }
        });
    }

    /// <summary>
    /// Modify Data that already Avaiable on Target
    /// </summary>
    /// <param name="reference"></param>
    /// <param name="itemKey"></param>
    /// <param name="updatedDatas"></param>
    /// <param name="newDataIfNotExist"></param>
    public void UpdateChildDatas(string reference, string itemKey, Dictionary<string, object> updatedDatas, bool newDataIfNotExist = false)
    {
        var currentReff = _database.GetReference(reference);
        currentReff.GetValueAsync().ContinueWith(task =>
        {
            if (task.Exception == null || newDataIfNotExist)
            {
                if (!task.Result.Child(itemKey).Exists)
                {
                    Debug.Log("Create New One");
                    //Create New One
                    foreach (var data in updatedDatas)
                    {
                        UpdateSpecifiedData(reference, itemKey + "/" + data.Key, data.Value, true);
                    }

                    return;
                }
                Debug.Log("Begin Update Data-Multiple");
                currentReff.Child(itemKey).UpdateChildrenAsync(updatedDatas);
                Debug.Log("Update Data-Multiple");
            }
            else
            {
                Debug.Log("Pass it-Update");
            }
        });
    }

    public void LoadData(string refernce, string itemKey, System.Action<DataSnapshot> OnDataGet, System.Action<string> onErrorHappen = null)
    {
        var curRef = _database.GetReference(refernce);
        curRef.Child(itemKey).GetValueAsync().ContinueWith(task =>
        {
            if (task.Exception != null) return;

            OnDataGet?.Invoke(task.Result);
        });
    }
    public void LoadData<T>(string refernce, string itemKey, System.Action<T> OnDataGet, System.Action<string> onErrorHappen = null)
    {
        var curRef = _database.GetReference(refernce);
        curRef.Child(itemKey).GetValueAsync().ContinueWith(task =>
        {
            if (task.Exception != null) return;
            var readData = task.Result.Value;

            if (readData is T)
            {
                OnDataGet?.Invoke((T)readData);
                return;
            }

            try
            {
                OnDataGet?.Invoke((T)System.Convert.ChangeType(readData, typeof(T)));
            }
            catch (System.InvalidCastException e)
            {
                Debug.Log(e);
            }


            //if (task.Exception != null) return;
            //T readData = (T) task.Result.Value;
            //OnDataGet?.Invoke(readData);
        });
    }

    #region Other Function
    public DatabaseReference RequestReference(string referenceAddress, string refrenceCallId)
    {
        if (!cachedReference.ContainsKey(refrenceCallId))
        {
            cachedReference[refrenceCallId] = _database.GetReference(referenceAddress);
        }
        return cachedReference[refrenceCallId];
    }
    public DatabaseReference RequestReferenceByAddress(string referenceAddress)
    {
        return _database.GetReference(referenceAddress);
    }
    #endregion
}
