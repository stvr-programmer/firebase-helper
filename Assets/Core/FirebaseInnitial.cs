using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseInnitial : MonoBehaviour
{
    Firebase.FirebaseApp app;
    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;
                Firebase.Analytics.FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
               //app.Options.DatabaseUrl = new System.Uri("https://fir-kit-9b0e7-default-rtdb.asia-southeast1.firebasedatabase.app/");
                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
                Debug.Log("Firebase Disable");
            }
        });
    }

   
}
