using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using UnityEngine.UI;

public class ExampleSaveLoad : MonoBehaviour
{
    public Button[] actionButton;

    #region SAMPLE CASE
    const string DB_REFKEY_USER = "USERS";
    #region SAVE DATA
    /*Case 1 Create or Save Data
     * 
     */
    const string user1_ID = "ABC-123";
    ModelData_User user1 = new ModelData_User()
    {
        nameUser = "user1_Name",
        address = "user1_Address"
    };
    public void SaveDataUser()
    {
        FB_RTDBHelper.Instance.SaveData(DB_REFKEY_USER, user1_ID, user1);
    }

    /*Case 2 Change Data of Firebase
     * 
     */
    ModelData_User user1_new = new ModelData_User()
    {
        nameUser = "user1_Name_New",
        address = "user1_Address_New"
    };
    public void UpdateDataUser()
    {
        FB_RTDBHelper.Instance.UpdateSpecifiedData(DB_REFKEY_USER, user1_ID, user1_new);
    }

    /*Case 3 Update data wich that data dosnt exists
    *
    */
    const string user2_ID = "DEF-345";
    ModelData_User user2 = new ModelData_User()
    {
        nameUser = "user2_Name",
        address = "user2_Address"
    };
    public void UpdateDataUserThatNotExist()
    {
        FB_RTDBHelper.Instance.UpdateSpecifiedData(DB_REFKEY_USER, user2_ID, user2, true);
    }

    /*Case 4 Update data Specified Child
   *'nameUser' are same as variable declaration name at ModelData_User.
   * So Be carefull when write it be.
   * Because it save as JSON wich have 'case sensitve'
   */
    string newName = "user1_Name_new2";
    string itemKey = user1_ID + "/" + "nameUser";
    public void UpdateDataUserSpecifiedChild()
    {
        FB_RTDBHelper.Instance.UpdateSpecifiedData(DB_REFKEY_USER, itemKey, newName);
    }

    /*Case 7 Adding List Data to Current User
     * We recomend using dictionary and use itemId as Key
     * We use 'userItems' as Key so it can convert to ModelData_UserWItems'
     */

    ModelData_UserItem item1 = new ModelData_UserItem()
    {
        itemId = "IT-01",
        itemName = "Item 1 Name",
        itemValue = 1,
        isEquiped = false
    };
    ModelData_UserItem item2 = new ModelData_UserItem()
    {
        itemId = "IT-02",
        itemName = "Item 2 Name",
        itemValue = 2,
        isEquiped = true
    };

    string itemDataKey = user1_ID + "/userItems";
    public void SaveNewDataItemToUser()
    {
        Dictionary<string, object> obtainedItem = new Dictionary<string, object>(){
            {item1.itemId,item1},
            {item2.itemId,item2}
        };

        FB_RTDBHelper.Instance.UpdateChildDatas(DB_REFKEY_USER, itemDataKey, obtainedItem, true);
    }

    /*Case 8 Modify Multiple Data on User
     * 
     */
    bool itemEquipStatus = true;
    int itemNewValue = 10;

    string item1RefKey { get { return item1.itemId; } }

    public void SaveUpdatedDataItemToUser()
    {
        Dictionary<string, object> updatedData = new Dictionary<string, object>()
        {
            { "isEquiped",itemEquipStatus},
            { "itemValue", itemNewValue}
        };
        FB_RTDBHelper.Instance.UpdateChildDatas(DB_REFKEY_USER + "/" + itemDataKey, item1RefKey, updatedData);
    }

    #endregion

    #region LOAD DATA
    /* Case 5 Get 1 user Data By Id
     *
     */
    public void LoadData_PickOneUser()
    {
        FB_RTDBHelper.Instance.LoadData(DB_REFKEY_USER, user1_ID, OnGetUser);
    }
    void OnGetUser(DataSnapshot datas)
    {
        string JSON = datas.GetRawJsonValue();
        ModelData_User user = new ModelData_User((ModelData_User)JsonUtility.FromJson<ModelData_User>(JSON));

        Debug.Log("This username are : " + user.nameUser);
        Debug.Log("This Address are : " + user.address);
    }

    /* Case 6 Get user1 specified Field. In This case are Address
    * 'address' are same as variable declaration name at ModelData_User.
   * So Be carefull when write it be.
   * Because it save as JSON wich have 'case sensitve'
    */
    string itemKey_load = user2_ID + "/" + "address";
    public void LoadData_UserSpecifiedField()
    {
        FB_RTDBHelper.Instance.LoadData<string>(DB_REFKEY_USER, itemKey_load, OnGetUserSpecifiedField);
    }
    void OnGetUserSpecifiedField(string fieldValue)
    {
        Debug.Log("this User Address Are : " + fieldValue);
    }

    #endregion

    #endregion

    #region Execute Sample
    private void Start()
    {
        

    }
    void TesterScript()
    {
       
    }
    #endregion
}
