using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelDataUse
{ }

[System.Serializable]
public struct ModelData_User
{
    public string nameUser;
    public string address;
    public List<ModelData_UserItem> userItems;

    public ModelData_User(ModelData_User newData)
    {
        nameUser = newData.nameUser;
        address = newData.address;
        userItems = newData.userItems;
    }
}
[System.Serializable]
public struct ModelData_UserWItem
{
    public string nameUser;
    public string address;
    public List<ModelData_UserItem> userItems;

    public ModelData_UserWItem(ModelData_UserWItem newData)
    {
        nameUser = newData.nameUser;
        address = newData.address;
        userItems = newData.userItems;
    }
}

[System.Serializable]
public struct ModelData_UserItem
{
    public string itemId;
    public string itemName;
    public int itemValue;
    public bool isEquiped;
}
